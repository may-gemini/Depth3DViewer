package com.zzh.depth3dviewer;

import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class Depth3DViewerActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 没有权限则授权
        if (!PermissionCheckUtil.checkRequiredPermissions(this)) {
            PermissionCheckUtil.requestAllPermissions(this);
            return;
        }
        GLSurfaceView glSurfaceView = new GLSurfaceView(this);
		setContentView(glSurfaceView);
        byte[] rawFileBytes = readPersonRawFile();
        if (rawFileBytes != null) {
            DepthImageRender myRender = new DepthImageRender();
            myRender.initImgVertices(rawFileBytes);
            glSurfaceView.setRenderer(myRender);
        } else {
            Log.e("Depth3DViewerActivity", "空数据！");
            finish();
        }
    }

    /**从深度图原图读取到字节数组，深度图为16位小端数据，深度数据在imagej中打开时，深度值小的越靠近相机，深度值大的远离相机**/
    public byte[] readPersonRawFile() {
        // 安卓12以下，depth_raw.raw文件放sdcard根目录
        String filePath = "/sdcard/depth_raw.raw";
        // 安卓12或以上版本，不需要申请文件访问权限，
        // 而且只能访问应用私有目录，例如/storage/emulated/0/Android/data/应用包名/files
        // depth_raw.raw文件放私有目录
        if (Build.VERSION.SDK_INT >= 30 && getExternalFilesDir(null) != null) {
            filePath = Objects.requireNonNull(getExternalFilesDir(null)).getPath() + File.separator + "depth_raw.raw";
        }
        try {
            return BufferUtil.toByteArray(new File(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
