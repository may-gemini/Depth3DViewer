package com.zzh.depth3dviewer;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

/**
 * add by zzh 20180726
 */
public class PermissionCheckActivity extends Activity {
    private static final String TAG = "PermissionCheckActivity";
    private static final int REQUIRED_PERMISSIONS_REQUEST_CODE = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, " onCreate" + savedInstanceState);
        if (savedInstanceState == null) {
            final String[] missingArray
                    = getIntent().getStringArrayExtra(com.zzh.depth3dviewer.PermissionCheckUtil.MISSING_PERMISSIONS);
            com.zzh.depth3dviewer.PermissionCheckUtil.setPermissionActivityCount(true);
            if (missingArray != null && missingArray.length > 0 && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                requestPermissionForM(missingArray);
            else {
                finish();
                returnOriginationActivity();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermissionForM(String[] missingArray) {
        requestPermissions(missingArray, REQUIRED_PERMISSIONS_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        finish();
        com.zzh.depth3dviewer.PermissionCheckUtil.setPermissionActivityCount(false);
        Log.d(TAG, " onRequestPermissionsResult Activity Count: "
                + com.zzh.depth3dviewer.PermissionCheckUtil.sPermissionsActivityStarted);
        if (com.zzh.depth3dviewer.PermissionCheckUtil.onRequestPermissionsResult(
                this, requestCode, permissions, grantResults, true)) {
            returnOriginationActivity();
        }
    }

    private void returnOriginationActivity() {
        try {
            Intent previousActivityIntent
                    = (Intent) getIntent().getExtras().get(
                    com.zzh.depth3dviewer.PermissionCheckUtil.PREVIOUS_ACTIVITY_INTENT);
            startActivity(previousActivityIntent);
        } catch (SecurityException e) {
            Log.e(TAG, " SecurityException happened: " + e);
        }
    }

    @Override
    public void finish() {
        super.finish();
        //关闭窗体动画显示
        this.overridePendingTransition(0, 0);
    }
}